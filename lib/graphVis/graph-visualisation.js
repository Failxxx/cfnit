const graphVis = {

    data: [],
    markers: [],
    defaultMessage: 'Veuillez selectioner plus de paramètres.',
    defaultRadius: 85,

    clearMarkers: function () {
        graphVis.markers = [];
    },

    setMarker: function (timestamp, type = 'claim') {

        let label = 'Claim/Check';
        if (type === 'valid') label = 'Check';
        if (type === 'claim') label = 'Claim';

        console.log({
            date: timestamp,
            type: type,
            label: label
        });

        graphVis.markers.push({
            date: timestamp,
            type: type,
            label: label
        });
    },

    getMarker: function (timestamp) {
        return graphVis.markers.filter(function (item) {
            return timestamp === item.date;
        });
    },

    existMarker: function (timestamp) {
        const test = graphVis.markers.find(function (item) {
            return timestamp === item.date;
        });
        return test !== undefined;
    },

    refreshData: function () {
        //Clear the graph before render
        graphVis.getGraph().innerHTML = "";
        if (graphVis.data.length === 1) graphVis.renderSoloCountry();
        else if (graphVis.data.length >= 2) graphVis.renderMultipleCountries();
        else graphVis.renderDefault();
    },

    renderSoloCountry: function () {
        console.log("DISPlAY - 1 pays");
        // Si un seul pays alors on affiche les indicateurs
        let data = graphVis.data[0];
        graphVis.addFlag(-1, data.flagURL, data.country);
        data.indicators.forEach(function (indicator, index) {
            let radius = graphVis.defaultRadius - 15 * index;
            graphVis.addRing(radius);
            graphVis.addIndicators(indicator, radius, "graph-bg-" + (index + 1));
            graphVis.addLabel(radius, indicator.name);
        });
    },
    renderMultipleCountries: function () {
        console.log("DISPlAY - au moins 2pays");
        // si il y a au moins 2 pays on les affiche et uniquement un seul indicateur par pays
        graphVis.addLabel(-1, graphVis.data[0].indicators[0].name);
        graphVis.data.forEach(function (country, index) {
            let radius = graphVis.defaultRadius - 15 * index;
            graphVis.addFlag(radius, country.flagURL, country.country);
            graphVis.addRing(radius);
            graphVis.addIndicators(country.indicators[0], radius, "graph-bg-1");
        });
    },
    renderDefault: function () {
        //reaction par defaut
        console.log("display default");
        if (graphVis.defaultMessage instanceof Element) graphVis.getGraph().append(graphVis.defaultMessage);
        else graphVis.getGraph().innerHTML = graphVis.defaultMessage;
    },

    getAngleRange: function (angleArray) {

        //console.log(angleArray);
        const max = angleArray.reduce(function (max, current) {
            return (current > max) ? current : max;
        }, angleArray[0]);

        const min = angleArray.reduce(function (min, current) {
            return (current < min) ? current : min;
        }, angleArray[0]);

        console.log(min, max);

        return {
            fromMin: min,
            fromMax: max
        };
    },
    getAngle: function (index, arrayLength) {
        return (parseFloat(index + 1) / parseFloat(arrayLength)) * 100.0;
    },
    getGraphContainerSize: function () {
        let graph = document.querySelector(".graph-container");
        let style = window.getComputedStyle(graph);
        let width = graph.offsetWidth - (parseFloat(style.paddingLeft) + parseFloat(style.paddingRight));
        let height = graph.offsetHeight - (parseFloat(style.paddingTop) + parseFloat(style.paddingBottom));
        return {height: height, width: width, ratio: height / width};
    },

    getGraphSize: function () {
        let graph = document.querySelector(".graph");
        let width = graph.offsetWidth;
        let height = graph.offsetHeight;
        return {height: height, width: width, ratio: height / width};
    },

    degreeToRadian: function (deg) {
        return deg * (Math.PI / 180);
    },

    getGraph: function () {
        return document.querySelector(".graph");
    },

    rescaleRange: function (numberToRescale, initScaleMin = 0, initScaleMax = 100, destinationMin = 5, destinationMax = 95) {
        numberToRescale = parseFloat(numberToRescale);
        initScaleMin = parseFloat(initScaleMin);
        initScaleMax = parseFloat(initScaleMax);
        destinationMin = parseFloat(destinationMin);
        destinationMax = parseFloat(destinationMax);
        return destinationMin + (((numberToRescale - initScaleMin) * (destinationMax - destinationMin)) / (initScaleMax - initScaleMin));
    },

    addIndicators: function (indicator, radius, colorClass = false) {

        let angleInPercent = 0;
        let angleRange = graphVis.getAngleRange(indicator.data.map(function (indicatorValue, index, array) {
            return graphVis.getAngle(index, array.length);
        }));

        for (let i = 0, item; item = indicator.data[i]; i++) {
            angleInPercent = graphVis.getAngle(i, indicator.data.length);
            //console.log("OnCountry - Angle: " + angle);
            if (item.percent !== undefined) graphVis.addPoint(item.percent, angleInPercent, radius, angleRange, colorClass, item.date);
            if (graphVis.existMarker(item.date) && radius === graphVis.defaultRadius) {
                const markers = graphVis.getMarker(item.date);
                markers.forEach(function (item) {
                    graphVis.addMarker(radius, angleInPercent, angleRange, item);
                });
            }
        }
    },

    addPoint: function (value = 100, angle, radiusInPercent, rescaleOptions = false, colorClass = false, date = '') {

        const pointMinSize = 5;
        const pointMaxSize = 20;

        if (rescaleOptions) angle = graphVis.rescaleRange(angle, rescaleOptions.fromMin, rescaleOptions.fromMax);
        //console.log(angle);
        let pointSize = graphVis.rescaleRange(parseFloat(value), 0, 100, pointMinSize, pointMaxSize);
        //console.log('point size ' + pointSize);

        let pointPos = graphVis.getCoordinates(angle, radiusInPercent, pointSize / 2.0, pointSize / 2.0);
        let parent = graphVis.getGraph();
        let pt = document.createElement('div');
        pt.classList.add("graph-point");
        pt.classList.add("fade-in");
        if (colorClass !== false) pt.classList.add(colorClass);
        pt.setAttribute('style', 'top: ' + pointPos.posY + 'px; left: ' + pointPos.posX + 'px; height:' + pointSize + 'px; width:' + pointSize + 'px; animation-delay: ' + (angle / 100) + 's;');
        //pt.setAttribute("title", value + "% le " + date);
        let inner = document.createElement('div');
        inner.classList.add('graph-point-label');
        inner.innerHTML = value + "%, " + graphVis.convertDate(date);
        pt.append(inner);
        parent.appendChild(pt);
    },

    convertDate: function timeConverter(timestamp, sep = '-') {
        const date = new Date(timestamp);
        const day = ('0' + date.getUTCDate()).slice(-2);
        const month = ('0' + (date.getUTCMonth() + 1)).slice(-2);
        const year = date.getUTCFullYear();
        return year + sep + month + sep + day;
    },

    getCoordinates: function (angleInPercent, radiusInPercent, shiftX = 7, shiftY = 7) {

        // In pixels
        let graphDimensions = graphVis.getGraphSize();

        // division by 200 because /100 for the percent then by 2 for the conversion fom the diameter to the radius
        let radius = (graphDimensions.height * radiusInPercent) / 200;
        let angle = graphVis.degreeToRadian(180 - (angleInPercent * 3.6));
        let posX = radius * Math.sin(angle);
        let posY = radius * Math.cos(angle) / graphDimensions.ratio;

        // recenter pts on the middle of the graph
        posX += (graphDimensions.width / 2) - shiftX;
        posY += (graphDimensions.height / 2) - shiftY;

        return {posX: posX, posY: posY};
    },

    addRing: function (size = 80) {

        let graph = graphVis.getGraph();
        let ring = document.createElement("div");
        ring.classList.add("graph-ring");
        ring.classList.add("fade-in");
        ring.setAttribute("style", "height: " + size + "%; width: " + size + "%;");
        graph.append(ring);
    },

    addFlag: function (radius = -1, flagURL = '', country = '') {

        // If the radius is -1 then the flag is placed in the middle of the visualisation

        let graph = graphVis.getGraph();
        let flag = document.createElement("div");
        flag.classList.add("graph-flag");
        flag.classList.add("fade-in");

        if (radius > 0) {
            let flagPos = graphVis.getCoordinates(1, radius, 40, 15);
            // Mise en commentaire du left pour les flags plus utile
            // Left: " + flagPos.posX + "px;
            flag.setAttribute("style", "top: " + flagPos.posY + "px; background-image:url(\"" + flagURL + "\");");
        } else {
            flag.setAttribute("style", "background-image:url(\"" + flagURL + "\"); transform: scale(2)");
        }

        flag.setAttribute("title", country);
        graph.append(flag);
    },

    addLabel: function (radius = 0, content) {

        let graph = graphVis.getGraph();
        let label = document.createElement("div");

        if (radius > 0) {
            let labelPos = graphVis.getCoordinates(0, radius, 50, 25);
            label.setAttribute("style", "top: " + labelPos.posY + "px; left: " + labelPos.posX + "px;");
            label.classList.add("graph-label-small");
        } else
            label.classList.add("graph-label");

        label.classList.add("fade-in");
        label.innerHTML = content;
        label.setAttribute("title", content);
        graph.append(label);
    },

    addMarker: function (radiusPoint, angle, rescaleOptions = false, markerOptions = false) {

        if (rescaleOptions) angle = graphVis.rescaleRange(angle, rescaleOptions.fromMin, rescaleOptions.fromMax);
        //console.log("Add marker: " + radius + " / " + angle);

        // The marker length is the min diment of the graph divided / 4
        let minDim;
        let dimensions = graphVis.getGraphSize();
        if (dimensions.height > dimensions.width) minDim = dimensions.width;
        else minDim = dimensions.height;
        let markerHeight = minDim / 5;

        let graph = graphVis.getGraph();
        let marker = document.createElement("div");
        let markerPos = graphVis.getCoordinates(angle, radiusPoint, 1.0, 0);
        marker.setAttribute("style", "height:" + markerHeight + "px;top: " + markerPos.posY + "px; left: " + markerPos.posX + "px; transform: rotate(" + angle * 3.6 + "deg);");
        marker.classList.add("graph-marker");
        marker.classList.add('graph-marker-' + markerOptions.type);
        marker.classList.add("fade-in");
        marker.setAttribute("title", "Apparition de la fake new");
        graph.append(marker);

        let radius = 100;
        switch (markerOptions.type) {
            case 'claim':
                radius = 100;
                break;
            case 'valid':
                radius = 110;
                break;
        }
        //console.log('Marker Radius : ' + radius);

        let labelPos = graphVis.getCoordinates(angle, radius, 25, 8);
        let label = document.createElement('div');
        label.classList.add('graph-marker-label');
        label.setAttribute("style", "top: " + labelPos.posY + "px; left: " + labelPos.posX + "px;");
        label.innerHTML = markerOptions.label;
        label.classList.add('graph-marker-label-' + markerOptions.type);

        graph.append(label);

    },

    /**
     * @desc Use to make the graph squared at any time, and simplify the placement
     * @return void
     */
    resizeGraphContainer: function () {
        let dimensions = graphVis.getGraphContainerSize();
        let minDim;

        if (dimensions.height > dimensions.width) minDim = dimensions.width;
        else minDim = dimensions.height;

        let graph = graphVis.getGraph();
        graph.setAttribute('style', 'height: ' + minDim + 'px; width:' + minDim + 'px;');
        //console.log("resize to " + minDim);
    },

};

/**
 * ---------- INIT ------------------------
 */

window.addEventListener('resize', graphVis.resizeGraphContainer);
window.addEventListener('resize', graphVis.refreshData);
graphVis.resizeGraphContainer();
graphVis.refreshData();