let generate_data_vis_timer = null;
const wrong_selection_message = '<div class="wrong-selection-container">\
<p class="wrong-selection-title">Wrong selection<p>\
<p class="wrong-selection-message">Please select one country with multiple indicators \
or multiple countries and one indicator.</p></div>';
const start_message = '<div class="wrong-selection-container">\
<p class="wrong-selection-title">How to start?<p>\
<p class="wrong-selection-message">Select one country with multiple indicators \
or multiple countries and one indicator.</p></div>';
const all_divs_loading_animation = new Array(34).fill("<div></div>").reduce((acc, div) => div + acc, "");
const loading_animation_container = '<div class="loadingio-spinner-spinner-5fnd7o4fife"><div class="ldio-0ptmoeul4gg">' + all_divs_loading_animation + '</div></div>';
graphVis.defaultMessage = start_message;
graphVis.refreshData();

const visualize = () => {
    const sel_indics = getSelectedIndicators(document.querySelector('.covid-indicators-list'));
    const sel_countries = getSelectedCountries(document.querySelector('.available-countries-list'));
    const sel_daterange = getSelectedDateRange(document.querySelector('.daterange-slider'));
    const daterange = sel_daterange[0] + '-' + sel_daterange[1];
    generateDataVisualization(sel_indics, sel_countries, daterange);
}

const generateDataVisualization = (sel_indics, sel_countries, daterange) => {
    const timeout_duration_visualization = 3000;
    const timeout_duration_clean = 1000;
    clearTimeout(generate_data_vis_timer); // timer stops
    graphVis.data = [];
    graphVis.defaultMessage = loading_animation_container;
    graphVis.refreshData();
    if((sel_indics.length === 1 && sel_countries.length > 0) || (sel_indics.length > 0 && sel_countries.length === 1)) {
        // timer starts
        generate_data_vis_timer = setTimeout(() =>
            getVisualizationData(sel_indics, sel_countries, daterange)
                .then((response) => graphVis.data = response)
                .then(() => { setMarkers(); })
                .then(() => { graphVis.refreshData(); })
                .catch((error) => console.log(error)),
                timeout_duration_visualization
        );
    }
    else {
        generate_data_vis_timer = setTimeout(() => {
            graphVis.data = [];
            graphVis.defaultMessage = (sel_indics.length === 0 && sel_countries.length === 0) ? start_message : wrong_selection_message;
            graphVis.refreshData();
        }, timeout_duration_clean);
    }
}

const setMarkers = () => {
    graphVis.clearMarkers();
    const claim = document.querySelector('.selected-article').firstChild.value;
    if(claim !== undefined && claim !== null) {
        if(claim.claimDate !== undefined && claim.claimDate !== null)
            graphVis.setMarker(surveyDateToTimestamp(formatDate((new Date(claim.claimDate)).getTime(), '')), type = 'claim');
        if(claim.claimReview[0].reviewDate !== undefined && claim.claimReview[0].reviewDate !== null)
            graphVis.setMarker(surveyDateToTimestamp(formatDate((new Date(claim.claimReview[0].reviewDate)).getTime(), '')), type = 'valid');
    }
}

const getSelectedIndicators = (list_container) =>
    Object.entries(list_container.children)
        .filter((el) => el[1].nodeName === 'UL')
        .map((ul) => Object.entries(ul[1].children).map((li) => li[1].children[0]))
        .reduce((tab, inputs) => tab.concat(inputs), [])
        .filter((checkbox) => checkbox.checked)
        .map((checkbox) => checkbox.value);

const getSelectedCountries = (list_container) =>
    Object.entries(list_container.children)
        .map((li) => li[1].children[0])
        .filter((checkbox) => checkbox.checked)
        .map((checkbox) => checkbox.value);