const buildCountryList = (countries, country_codes) => {
    const main_container = document.querySelector('.countries-list');
    if(countries !== null && countries !== undefined) {
        // list
        const ordered_list = document.createElement('ol');
        ordered_list.name = 'available_countries';
        ordered_list.className = 'available-countries-list';

        main_container.appendChild(ordered_list);
        // list items
        countries.forEach((el) => {
            // list item
            const list_item = document.createElement('li');
            // label
            const label = document.createElement('label');
            label.setAttribute('for', el.country);
            label.innerHTML = el.country;
            // Logo checkbox 
            const logo_checkbox = document.createElement('img');
            logo_checkbox.className = 'not-checked-logo';
            logo_checkbox.alt = 'Checked';
            logo_checkbox.src = 'res/checkmark.svg';
            // checkbox
            const checkbox = document.createElement('input');
            checkbox.type = 'checkbox';
            checkbox.id = el.country;
            checkbox.value = el.country;
            checkbox.name = el.country;
            checkbox.setAttribute('onclick', 'onClickCheckbox(this)');
            // country flag
            // const flag = document.createElement('img');
            // flag.setAttribute('src', fpAPI.getFlagUrl(country_codes[el.country]));
            // flag.setAttribute('alt', el.country);
            // flag.setAttribute('width', '40px');

            list_item.appendChild(checkbox);
            list_item.appendChild(logo_checkbox);
            list_item.appendChild(label);
            // list_item.appendChild(flag);
            ordered_list.appendChild(list_item);
        });
    }
    else {
        const error_message = document.createElement('p');
        error_message.className = 'countries-list-error-message';
        error_message.innerHTML = 'Error : no country available ...';
        main_container.appendChild(error_message);
    }

}