const buildIndicatorsList = (indicators) => {
    const main_container = document.querySelector('.covid-indicators-list');
    indicators.map((set_of_indics) => {
        // sublist container
        const sublist_container = document.createElement('ul');
        sublist_container.name = set_of_indics.category;
        // Label container
        const label_container = document.createElement('div');
        label_container.className = 'label-container';
        label_container.setAttribute('onclick', 'onClickDropDown(this)');
        // label sublist
        const label_sublist = document.createElement('label');
        label_sublist.setAttribute('for', set_of_indics.category);
        label_sublist.innerHTML = set_of_indics.category;
        // dropdown logo
        const logo_dropdown = document.createElement('img');
        logo_dropdown.className = 'logo-dropdown';
        logo_dropdown.alt = 'dropdown';
        logo_dropdown.src = 'res/arrow-down.svg';

        
        label_container.appendChild(label_sublist);
        label_container.appendChild(logo_dropdown);
        // list items
        const indicators = set_of_indics.indicators.map((indic) => {
            // list item
            const list_element = document.createElement('li');
            list_element.title = indic.description;
            // label checkbox
            const label_checkbox = document.createElement('label')
            label_checkbox.setAttribute('for', indic.indicator);
            label_checkbox.innerHTML = indic.name[0].toUpperCase() + indic.name.slice(1);
            // Logo checkbox 
            const logo_checkbox = document.createElement('img');
            logo_checkbox.className = 'not-checked-logo';
            logo_checkbox.alt = 'Checked';
            logo_checkbox.src = 'res/checkmark.svg';
            // checkbox
            const checkbox = document.createElement('input');
            checkbox.type = 'checkbox';
            checkbox.id = indic.indicator;
            checkbox.value = indic.indicator;
            checkbox.name = indic.indicator;
            checkbox.setAttribute('onclick', 'onClickCheckbox(this)');

            list_element.appendChild(checkbox);
            list_element.appendChild(logo_checkbox);
            list_element.appendChild(label_checkbox);
            return list_element;
        });
        indicators.forEach((indic) => { sublist_container.appendChild(indic); });
        return { container: sublist_container, label: label_container };
    })
    .forEach((sublist) => {
        main_container.appendChild(sublist.label);
        main_container.appendChild(sublist.container);
    });
};

const onClickCheckbox = (node) => {
    const parent = node.parentElement;
    parent.classList.toggle("covid-sublist-container-checked");
    const checked_logo = parent.firstChild.nextSibling;
    checked_logo.classList.toggle("checked-logo");
    visualize();
}

const onClickDropDown = (node) => {
    node.firstChild.nextSibling.classList.toggle('rotated-img');
    const sibling = node.nextSibling;
    sibling.classList.toggle('covid-sublist-container');
}