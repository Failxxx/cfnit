class cwsodAPI {

    static indicators;

    static getAvailableCountries = async () => {
        return fetch('https://covidmap.umd.edu/api/country')
            .then((response) => response.json())
            .then((data) => data.data)
            .catch((error) => console.log(error));
    };

    static getDataOnIndicator = async (params = {indicator, country, daterange, type}) => {
        const url = Object.entries(params)
            .reduce((acc, field) => acc + '&' + field[0] + '=' + field[1], 'https://covidmap.umd.edu/api/resources?');
        return fetch(url)
            .then((response) => response.json())
            .catch((error) => console.log(error));
    };

    static getIndicDataByIndicId = (id) => {
        for(let category of this.indicators)
            for(let indicator of category.indicators)
                if(indicator.indicator === id)
                    return { data: { indicator: indicator }, error: null };
        return { data: null, error: {id: 1, message: "No indicator found for id: " + id } };
    };

    // static getCategories = async () => {
    //     /* ----- WARNING ----- */
    //     // This is an old version of getting the indicators (before 2021-05-06)
    //     return fetch("src/js/cwsod_api/categories.json")
    //         .then((response) => response.json())
    //         .catch((error) => console.log(error));
    // };

    // static getIndicators = async () => {
    //     /* ----- WARNING ----- */
    //     // This is an old version of getting the indicators (before 2021-05-06)

    //     // This method voluntarily triggers a bad request in order to get the list of indicators
    //     const categories = Object.entries(await this.getCategories());
    //     const wrong_params = {indicator:'all', country:'France', daterange:'20210401-20210410', type:'daily'};
    //     const indicators = await this.getDataOnIndicator(wrong_params);
    //     const around_message = {beginning: 'need parameter:[indicator: ', end:']'};
    //     const response = indicators.error
    //         .slice(around_message.beginning.length)
    //         .split(' or')
    //         .map((el) => el.slice(1, el.length - 1))
    //         .map((el) => {
    //             const beg_index_char = el.indexOf("'", 0);
    //             const end_index_char = el.indexOf("'", el.length/2);
    //             if(beg_index_char === end_index_char && beg_index_char === -1)
    //                 return el;
    //             if(beg_index_char === end_index_char && beg_index_char !== -1)
    //                 return el.slice(0, end_index_char - 1);
                
    //             let length = el.length;
    //             if(beg_index_char !== -1)
    //                 el = el.slice(beg_index_char + 1, length);
    //             if(end_index_char !== -1)
    //                 el = el.slice(0, end_index_char - 1);
    //             return el;
    //         });
    //     return categories.map((el) => {
    //         return {'category': el[0], 'indicators': el[1].map((nb_indic) => response[nb_indic])};
    //     });
    // };

};