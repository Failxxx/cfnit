// Find JSR documentation here : https://soanvig.github.io/mm-jsr/api/index.html

const formatDate = (timestamp, sep = '-') => {
    const date = new Date(timestamp);
    const day = ('0' + date.getUTCDate()).slice(-2);
    const month = ('0' + (date.getUTCMonth() + 1)).slice(-2);
    const year = date.getUTCFullYear();
    return year + sep + month + sep + day;
};

const daterange_slider = new JSR({
    modules: [
        new JSR.Rail(),
        new JSR.Slider(),
        new JSR.Bar(),
        new JSR.Label({ formatter: formatDate}),
        new JSR.Grid({ formatter: formatDate, color: '#999'}),
      ],
      config: {
        min: 1575158400000, // 1575158400000 <==> 20191201 00:00:00 0000
        max: Date.now() - 1000 * 60 * 60 * 24,
        step: 1000 * 60 * 60 * 24,
        initialValues: [Date.now() - 4*7 * 24 * 60 * 60 * 1000, Date.now() - 7 * 24 * 60 * 60 * 1000],
        container: document.querySelector('.daterange-slider'),
      }
});

let choose_value_daterange_timeout = null;
daterange_slider.onValueChange(() => {
  clearTimeout(choose_value_daterange_timeout);
  choose_value_daterange_timeout = setTimeout(() => visualize(), 1000);
});

const getSelectedDateRange = () =>
    daterange_slider.engine.stateProcessor.state.values.map((value) => 
        formatDate(value.real).split('-').reduce((daterange, date) => daterange + date));