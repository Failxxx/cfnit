const buildSearchResults = (results, rows = 3, columns = 3) => {
    const main_container = document.querySelector('.gfct-search-results');
    const more_results = document.querySelector('.more-results-btn');
    if(results.error === null){
        for(let r = 0; r < rows; r++) {
            const row = document.createElement('div');
            row.className = 'search-results-row';
            for(let c = 0; c < columns; c++)
                if(results.data.claims[r * rows + c] !== undefined)
                    row.appendChild(buildNews(results.data.claims[r * rows + c]));
            main_container.appendChild(row);
        }
        main_container.style.display = 'flex';

        // more results button
        if(results.data.nextPageToken !== undefined) {
            more_results.style.display = 'block';
            more_results.setAttribute('onClick', 'handleOnClickNextPageButton("' + results.data.nextPageToken + '")');
        } else
            more_results.style.display = 'none';

    }
    else if (results.error.code === 1) {
        main_container.style.display = 'none';
        more_results.style.display = 'none';
    }
    else if (results.error.code === 2) {
        const message = document.createElement('p');
        message.className = 'search-results-error-message';
        message.innerHTML = results.error.message;
        main_container.appendChild(message);
        main_container.style.display = 'flex';

        more_results.style.display = 'none';
    }

}

const buildNews = (claim, button = 'select') => {
    // news container
    const news_container = document.createElement('div');
    news_container.className = 'news-container';
    news_container.value = claim;
    if(claim !== null && claim !== undefined) {
        // publisher
        const publisher = document.createElement('div');
        publisher.className = 'news-publisher-container';
        const publisher_label = document.createElement('p');
        publisher_label.className = 'news-publisher-label';
        publisher_label.innerHTML = 'Publisher : ';
        const publisher_name = document.createElement('a');
        publisher_name.className = 'news-publisher-name';
        publisher_name.innerHTML = claim.claimReview[0].publisher.name;
        publisher_name.setAttribute('href', 'https://' + claim.claimReview[0].publisher.site);
        publisher_name.setAttribute('target', 'blank');
        publisher.appendChild(publisher_label);
        publisher.appendChild(publisher_name);
        news_container.appendChild(publisher);
        // title
        const title = document.createElement('p');
        title.className = 'news-title';
        title.innerHTML = (claim.text.length > 100) ? claim.text.slice(0, 90) + '...' : claim.text;
        news_container.appendChild(title);
        // dates
        const dates_container = document.createElement('div');
        dates_container.className = 'news-dates-container';
        // claim date
        const claim_date_container = document.createElement('div');
        claim_date_container.className = 'news-claim-date-container';
        const logo_claim_date = document.createElement('img');
        logo_claim_date.className = 'news-claim-date-logo';
        logo_claim_date.alt = 'Claim date illustration';
        logo_claim_date.src = 'res/error.svg';
        const claim_date = document.createElement('p');
        claim_date.className = 'claim-date';
        const c_date = (claim.claimDate !== undefined) ? claim.claimDate.slice(0, 10) : 'no data';
        claim_date.innerHTML = 'Claim : ' + c_date;
        claim_date_container.appendChild(logo_claim_date);
        claim_date_container.appendChild(claim_date);
        dates_container.appendChild(claim_date_container);
        // review date
        const review_date_container = document.createElement('div');
        review_date_container.className = 'news-review-date-container';
        const logo_review_date = document.createElement('img');
        logo_review_date.className = 'news-review-date-logo';
        logo_review_date.alt = 'Review date illustration';
        logo_review_date.src = 'res/checkmark.svg';
        const review_date = document.createElement('p');
        review_date.className = 'news-review-date';
        const r_date = (claim.claimReview[0].reviewDate !== undefined) ? claim.claimReview[0].reviewDate.slice(0, 10) : 'no data';
        review_date.innerHTML = 'Review : ' + r_date;
        review_date_container.appendChild(logo_review_date);
        review_date_container.appendChild(review_date);
        dates_container.appendChild(review_date_container);

        news_container.appendChild(dates_container);
        const element_container = document.createElement('div');
        element_container.className = 'news-elements-container';
        const element_left_container = document.createElement('div');
        element_left_container.className = 'news-left-elements-container';
        element_container.appendChild(element_left_container);
        // rating
        const rating = document.createElement('p');
        rating.className = 'news-review-rating';
        rating.innerHTML = claim.claimReview[0].publisher.name + ' rating : ' + claim.claimReview[0].textualRating;
        element_left_container.appendChild(rating);
        // view article
        const view_article = document.createElement('a');
        view_article.className = 'news-review-view-article';
        view_article.innerHTML = 'View article';
        view_article.setAttribute('href', claim.claimReview[0].url);
        view_article.setAttribute('target', 'blank');
        element_left_container.appendChild(view_article);
        if(button === 'select') {
            // select article button
            const select_article = document.createElement('button');
            select_article.className = 'article-button-select';
            select_article.innerHTML = 'Select';
            select_article.setAttribute('onClick', 'handleOnClickSelectButton(this.parentNode.parentNode)');
            element_container.appendChild(select_article);
        }
        if(button === 'change') {
            // change article button
            const change_article = document.createElement('button');
            change_article.className = 'article-button-change';
            change_article.innerHTML = 'Change';
            change_article.setAttribute('onClick', 'handleOnClickChangeButton()');
            element_container.appendChild(change_article);
        }

        news_container.appendChild(element_container);
    }
    else {
        const container = document.createElement('div');
        container.className = 'no-article-container';
        const message = document.createElement('p');
        message.className = 'no-article-selected-message';
        message.innerHTML = 'Please, select a fake news';
        container.appendChild(message);
        const change_article = document.createElement('button');
        change_article.className = 'article-button-change';
        change_article.innerHTML = 'Change';
        change_article.setAttribute('onClick', 'handleOnClickChangeButton()');
        container.appendChild(change_article);
        news_container.appendChild(container);
    }

    return news_container;
}