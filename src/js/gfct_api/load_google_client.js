const loadGoogleClient = async () => {
    gapi.load('client', {
        callback: () => {
            gapi.client.setApiKey(keys[GFCT_API]);
            return gapi.client.load('https://content.googleapis.com/discovery/v1/apis/factchecktools/v1alpha1/rest')
                .then(() => console.log('%cGAPI client loaded for API', green_log),
                (error) => console.log('%cError loading GAPI client for API', red_log));
        },

        onerror: () => {
            alert('gapi.client failed to load');
        },

        timeout: 5000,

        ontimeout: () => {
            alert('gapi.client could no load in a timely manner');
        }
    })
};