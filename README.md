# Covid Fake News Impact Tool (CFNIT)

## Summary
Nowadays, information and misinformation spread more and more rapidly because of new media and technologies. Fake news is taking much more space in the media especially with the covid crisis. Our work focuses on the spreading of these fake news and how it may affect populations.

We have built a fully functional application that merges two datasets. Our tool proposes a way to put fake news and social or health indicators side by side. It provides a new approach to visualize if certain fake news have an impact on populations.

## Datasets

* Google Fact Check Tools API : https://developers.google.com/fact-check/tools/api/?hl=fr
* COVID-19 World Survey Open Data API : https://gisumd.github.io/COVID-19-API-Documentation/docs/home.html

## API keys
Here is how to tell the application to use your api keys :
* Create a new file under `src/js/` named `api_keys.json`
* Copy and paste this :
  ```json
    {
      "gfct_api": "YOUR_API_KEY",
      "comments": "gfct_api = Google Fact Check Tools API, cwsod_api = COVID-19 World Survey Open Data API"
    }
  ```
  
## Run the application
* Install python
* Open a new terminal
* Start the server :
  ```python
    #python 2
    python -m SimpleHTTPServer 8000
    #python 3
    python -m http.server 8000
  ```
* Then, open a web browser at `http://localhost:8000`